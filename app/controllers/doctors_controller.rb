class DoctorsController < ApplicationController

    # GET method to get all Doctor from database (/doctors/list)
    def index
        @doctors = Doctor.all
        render json: @doctors, status: :ok
    end

    # GET method to get a Doctor by id
    def show
        @doctors = Doctor.find(params[:id])
    end
    
end

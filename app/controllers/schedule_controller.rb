class ScheduleController < ApplicationController

    # POST method to booking doctor schedule
    def create
        @listGuestbook = GuestbookDoctor.where({scheduleId: params[:scheduleId], dateAppointment: params[:dateAppointment]})
        
        if (@posts.size > 10) 
            render json: { errors: 'The schedule has been full booking' },
                status: :unprocessable_entity
        end

        @doctorSchedule = doctorSchedule.find(params[:scheduleId])

        @now = Time.new

        @time1 = now + 30*60
        @hour = time1.strftime("%H:%M:%S")
        @day = now.strftime("%A")

        if (day != doctorSchedule.day)
            render json: { errors: 'The schedule is invalid' },
                status: :unprocessable_entity
        end

        if (hour > doctorSchedule.endTime)
            render json: { errors: 'Sorry, The patient registration is closed' },
                status: :unprocessable_entity
        end
        
        @guestbook = GuestbookDoctor.new(guestbook_params)
        if @guestbook.save
            render json: @guestbook, status: :created
        else
            render json: { errors: @guestbook.errors.full_messages },
                status: :unprocessable_entity
        end
    end

    # GET method to get all Doctor Schedule with patient booking data
    def showGuestbook
        @listGuestbook = GuestbookDoctor.joins(doctorSchedule:[:doctors],:users)

        @data = []
        while listGuestbook.each do |guest|
            data[] = [
                date: guest.strftime("%Y-%m-%d"),
                doctor: guest.doctorSchedule.doctors.doctorName,
                hospital: guest.doctorSchedule.hospitals.hospitalName,
                patient: guest.users.name
            ]
        end

        render json: @data, status: :ok
    end
    

    def guestbook_params
        params.require(:guestbook).permit(:scheduleId, :userId, :dateAppointment, :seqNumber)
    end
end

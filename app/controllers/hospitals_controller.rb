class HospitalsController < ApplicationController
    
    # GET method to get all Hospital from database (/hospitals/list)
    def index
        @hospitals = Hospital.all
        render json: @hospitals, status: :ok
    end

    # GET method to get a Hospital by id
    def show
        @hospitals = Hospital.find(params[:id])
    end
    
end

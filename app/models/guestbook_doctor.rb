class GuestbookDoctor < ApplicationRecord
    validates :scheduleId, presence: true
    validates :userId, presence: true
    validates :dateAppointment, presence: true
    validates :seqNumber, presence: true

    belongs_to :doctor_schedule
    belongs_to :user
end

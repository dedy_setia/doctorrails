class Hospital < ApplicationRecord
    validates :hospitalName, presence: true
    validates :hospitalAddress, presence: true
    validates :hospitalPhone, presence: true
end

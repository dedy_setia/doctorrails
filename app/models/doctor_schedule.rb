class DoctorSchedule < ApplicationRecord
    validates :hospitalId, presence: true
    validates :doctorId, presence: true
    validates :day, presence: true
    validates :startTime, presence: true
    validates :endTime, presence: true

    belongs_to :hospital
    belongs_to :doctor
end

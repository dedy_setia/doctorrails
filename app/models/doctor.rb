class Doctor < ApplicationRecord
    validates :doctorName, presence: true
    validates :doctorTitle, presence: true
    validates :doctorPhone, presence: true
end

# README

Technical Test SehatQ with Ruby on Rails:

* System dependencies
    - ruby-2.6.3
    - Rails 6.0.3
    - Json Web Token (JWT)
    - SQLite3
    - Bcrypt

* Endpoint
    - User Registration : POST /user
    - User Login        : POST /login
    - Get Hospital      : GET /hospitals/list
    - Get Doctor        : GET /doctors/list
    - Booking Doctor    : POST /schedule/create
    - Get Doctor Schedule    : GET /schedule/listdoctor

* Deployment instructions

    - Clone this project:
        $ git clone https://dedy_setia@bitbucket.org/dedy_setia/doctorrails.git

    - Change directory to this project: 
        $ cd doctorrails

    - Install dependencies:
        $ bundle install
    
    - Create database:
        $ rake db:create

    - Run migration:
        $ rake db:migrate

    - Run this project:
        $ rails server



class CreateDoctorSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :doctor_schedules do |t|
      t.integer :hospitalId
      t.integer :doctorId
      t.string :day
      t.time :startTime
      t.time :endTime

      t.timestamps
    end
  end
end

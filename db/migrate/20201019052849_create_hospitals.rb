class CreateHospitals < ActiveRecord::Migration[6.0]
  def change
    create_table :hospitals do |t|
      t.string :hospitalName
      t.string :hospitalAddress
      t.string :hospitalPhone

      t.timestamps
    end
  end
end

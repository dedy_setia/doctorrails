class CreateGuestbookDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :guestbook_doctors do |t|
      t.integer :scheduleId
      t.integer :userId
      t.date :dateAppointment
      t.integer :seqNumber

      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resource :users, only: [:create]
  post "/login", to: "users#login"
  get "/auto_login", to: "users#auto_login"

  get "/hospitals/list", to: "hospitals#index"
  get "/doctors/list", to: "doctors#index"

  post "/schedule/create", to: "schedule#create"
  get "/schedule/listdoctor", to: "schedule#showGuestbook"

end
